﻿using UnityEngine;
using System.Collections;

public class MoveMenuLight : MonoBehaviour {

    private Vector3 mousePos;
    public float zValue;
    // Use this for initialization
	void Start () {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mousePos.x, mousePos.y, zValue);

	}
	
	// Update is called once per frame
	void Update () {
        mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        transform.position = new Vector3(mousePos.x, mousePos.y, zValue);
	}
}
