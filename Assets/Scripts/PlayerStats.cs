﻿using UnityEngine;
using System.Collections;

public class PlayerStats : MonoBehaviour {

    public float distanceToGoal;
    private GameObject goal;

    //public float minPosX;
    //public float maxPosX;


    public float DistanceToGoal
    {
        set { distanceToGoal = value; }
        get { return distanceToGoal; }
    }


    // Use this for initialization
	void Awake () {
        goal = GameObject.Find("Goal");
        distanceToGoal = CalculateDistanceToTarget(goal);
	}
	
	// Update is called once per frame
	void Update () {

        //float temp = transform.position.x;

        //temp = Mathf.Clamp(temp, minPosX, maxPosX);
        //transform.position = new Vector3(temp, transform.position.y, transform.position.z);


        distanceToGoal = CalculateDistanceToTarget(goal);
	}

    float CalculateDistanceToTarget(GameObject t)
    {
        return t.transform.position.x - transform.position.x;
    }
}
