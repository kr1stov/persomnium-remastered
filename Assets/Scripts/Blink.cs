﻿using UnityEngine;
using System.Collections;

public class Blink : MonoBehaviour {

    Animator anim;

    static int idleState = Animator.StringToHash("Base Layer.Idle");

    
    // Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
	}

    void FixedUpdate()
    {
        AnimatorStateInfo currentBaseState = anim.GetCurrentAnimatorStateInfo(0);
        //Debug.Log("current: " + currentBaseState.nameHash + "        idle: " + idleState);

        if (currentBaseState.nameHash == idleState)
        {
            //Debug.Log("EQUAL");
            int r = Random.Range(0, 100);
            
            if(r < 1)
            {
                anim.SetTrigger("Blink");
            }
        }
    }
}
