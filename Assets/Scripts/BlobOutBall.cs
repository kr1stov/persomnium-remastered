﻿using UnityEngine;
using System.Collections;

public class BlobOutBall : MonoBehaviour {

    public GameObject ball;

    private Animator anim;
    // Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        InvokeRepeating("SpawnBall", 0, 3);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void SpawnBall()
    {
        anim.SetTrigger("Blob");
        Instantiate(ball, transform.position, Quaternion.identity);
    }
}
