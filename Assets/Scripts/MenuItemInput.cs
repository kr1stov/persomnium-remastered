﻿using UnityEngine;
using System.Collections;

public class MenuItemInput : MonoBehaviour
{
    public AudioClip toggleSound;
    public AudioClip selectSound;

    void Start()
    {
        PlayerPrefs.DeleteKey("psr_lastSpawnX");
        PlayerPrefs.DeleteKey("psr_lastSpawnY");
    }

    void OnMouseEnter()
    {
        audio.PlayOneShot(toggleSound);
    }

    void OnMouseOver()
    {
        //renderer.material.color = new Color(255f, 255f, 255f);
        //transform.localScale = new Vector3(1.1f, 1.1f, 1.1f);
        transform.GetChild(0).gameObject.SetActive(true);
        transform.GetChild(1).gameObject.SetActive(true);

        if(Input.GetMouseButtonDown(0))
        {
            DoMenuItemStuff(gameObject.name);
            audio.PlayOneShot(selectSound);
        }
    }

    void OnMouseExit()
    {
        //renderer.material.color = new Color(128f, 128f, 128f);
        //transform.localScale = new Vector3(1.0f, 1.0f, 1.0f);
        transform.GetChild(0).gameObject.SetActive(false);
        transform.GetChild(1).gameObject.SetActive(false);
    }


    void DoMenuItemStuff(string name)
    {
        switch (name)
        {
            case "Start":
                Application.LoadLevel("Level");
                break;
            case "Options":
                
                break;
            case "Credits":
                
                break;
            case "End":
                Application.Quit();
                break;
            default:
                break;
        }
    }
}
