﻿using UnityEngine;
using System.Collections;
using System.IO;

public class ReadCreditsFile : MonoBehaviour {

    public float scrollSpeed;
    // Use this for initialization
	void Start () {
        StreamReader sr = new StreamReader("Assets\\Persomnium\\credits.txt");

        string lines = sr.ReadToEnd();

        guiText.text = lines;
	}
	
	// Update is called once per frame
	void Update () {
        guiText.pixelOffset += new Vector2(0, scrollSpeed * Time.deltaTime);
	}
}
