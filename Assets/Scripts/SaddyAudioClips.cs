﻿using UnityEngine;
using System.Collections;

public class SaddyAudioClips : MonoBehaviour {

    public enum SoundClips { Jump, Land, Walk, Death };

    public AudioClip[] jumpSounds;
    public AudioClip[] landSounds;
    public AudioClip[] walkSounds;
    public AudioClip[] deathSounds;
    

    public AudioClip GetRandomSound(SoundClips s)
    {
        int r;
       
        switch (s)
        {
            case SoundClips.Death:
                r = Random.Range(0, deathSounds.Length - 1);
                return deathSounds[r];
            case SoundClips.Walk:
                r = Random.Range(0, walkSounds.Length - 1);
                return deathSounds[r];
            case SoundClips.Jump:
                r = Random.Range(0, jumpSounds.Length - 1);
                return jumpSounds[r];
            case SoundClips.Land:
                r = Random.Range(0, landSounds.Length - 1);
                return landSounds[r];
            default:
                return null;
        }
    }


}
