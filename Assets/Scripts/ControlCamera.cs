﻿using UnityEngine;
using System.Collections;

public class ControlCamera : MonoBehaviour {

    public float startFollowX;
    public float stopFollowX;
    
    // Use this for initialization
	void Start () {
        //GetComponent<ControlCamera>().enabled = false;
	}
	
	// Update is called once per frame
	void Update () {
        if (GameObject.FindGameObjectWithTag("Player").transform.position.x <= startFollowX || GameObject.FindGameObjectWithTag("Player").transform.position.x >= stopFollowX)
        {
            GetComponent<FollowCam>().enabled = false;
        }
        else
        {
            GetComponent<FollowCam>().enabled = true;
            GetComponent<FollowCam>().target = GameObject.FindGameObjectWithTag("Player").transform;
        }
        
        //if(GameObject.FindGameObjectWithTag("Player").transform.position.x > startFollowX)
        //{

        //}

	
	}
}
