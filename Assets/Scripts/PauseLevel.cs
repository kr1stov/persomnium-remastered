﻿using UnityEngine;
using System.Collections;

public class PauseLevel : MonoBehaviour {

    public GUIText pauseText;

    private bool isPaused = false;

	// Update is called once per frame
	void Update () {
        if (Input.GetButtonDown("Pause"))
        {
            Debug.Log("PAUSE");
            isPaused = !isPaused;
        }


        if(isPaused)
        {
            pauseText.gameObject.SetActive(true);
            Time.timeScale = 0;
        }
        else
        {
            pauseText.gameObject.SetActive(false);
            Time.timeScale = 1;
        }
	
	}
}
