﻿using UnityEngine;
using System.Collections;
using System;



public class MakeScreenshot : MonoBehaviour {

    public GUIText scText;
    // Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Alpha1))
        {
            DateTime dt = DateTime.Now;

            string name = "Screenshots\\sc_" + 
                 dt.Year + 
                (dt.Month < 10 ? "0" + dt.Month : dt.Month.ToString()) + 
                (dt.Day < 10 ? "0" + dt.Day : dt.Day.ToString()) + "_" +
                (dt.Hour < 10 ? "0" + dt.Hour : dt.Hour.ToString()) +
                (dt.Minute < 10 ? "0" + dt.Minute : dt.Minute.ToString()) +
                (dt.Second < 10 ? "0" + dt.Second : dt.Second.ToString());
            
            name += ".jpg";

            Application.CaptureScreenshot(name);
            //Invoke("Show", 0.1f);
        }
	}

    void Show()
    {
        scText.gameObject.SetActive(true);
        Invoke("UnShow", .25f);
    }

    void UnShow()
    {
        scText.gameObject.SetActive(false);
    }
}
