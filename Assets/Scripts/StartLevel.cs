﻿using UnityEngine;
using System.Collections;

public class StartLevel : MonoBehaviour {

	// Use this for initialization
    public GameObject player;

    private Vector3 spawnPosition;
    private Transform firstSpawnPoint;

    
    void Awake () {
        GameObject help = GameObject.FindGameObjectWithTag("Player");

        if (!help)
        {

            firstSpawnPoint = GameObject.Find("Respawn0").transform;

            spawnPosition.x = PlayerPrefs.GetFloat("psr_lastSpawnX", firstSpawnPoint.position.x);
            spawnPosition.y = PlayerPrefs.GetFloat("psr_lastSpawnY", firstSpawnPoint.position.y);
            spawnPosition.z = firstSpawnPoint.position.z;

            GameObject temp = (GameObject)Instantiate(player, spawnPosition, Quaternion.identity) as GameObject;
            temp.name = temp.name.Remove(temp.name.Length - 7);
        }
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
