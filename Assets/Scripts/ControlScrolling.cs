﻿using UnityEngine;
using System.Collections;

public class ControlScrolling : MonoBehaviour {

	void OnTriggerEnter(Collider other)
    {
        Debug.Log("ENTERED");


        if(other.name == "End Zone")
        {
            GameObject[] gos = GameObject.FindGameObjectsWithTag("Level");

            foreach(GameObject g in gos)
            {
                Scroll s = g.GetComponent<Scroll>();

                if(s)
                {
                    s.enabled = false;
                }
            }
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.name == "End Zone")
        {
            GameObject[] gos = GameObject.FindGameObjectsWithTag("Level");

            foreach (GameObject g in gos)
            {
                Scroll s = g.GetComponent<Scroll>();

                if (s)
                {
                    s.enabled = true;
                }
            }
        }
    }
}
