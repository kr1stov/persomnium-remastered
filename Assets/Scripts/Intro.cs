﻿using UnityEngine;
using System.Collections;

public class Intro : MonoBehaviour {

    private AudioClip introMusic;
    public float introLength;
    public Texture2D[] introScenes;
    public int currentScene;
    public int numberOfScenes;
    
    public float timePerScene;
    public float timeToNextScene;

    // Use this for initialization
	void Start () {
        this.currentScene = 0;
        transform.position = Vector3.zero;
        transform.localScale = Vector3.zero;
        guiTexture.texture = this.introScenes[currentScene];

        this.numberOfScenes = this.introScenes.Length;

        this.introMusic = audio.clip;
        this.introLength = this.introMusic.length;
        this.timePerScene = this.introLength / this.numberOfScenes;

        this.timeToNextScene = this.timePerScene;
	}
	
	// Update is called once per frame
	void Update () {
        Debug.Log("time: " + Time.timeSinceLevelLoad);
        guiTexture.pixelInset = new Rect(0, 0, Screen.width, Screen.height);

        if((this.currentScene < this.introScenes.Length) && (Time.timeSinceLevelLoad > this.timeToNextScene))
        {
            guiTexture.texture = this.introScenes[++currentScene];
            this.timeToNextScene += this.timeToNextScene;
        }
	}
}
