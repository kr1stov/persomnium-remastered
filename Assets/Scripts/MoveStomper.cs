﻿using UnityEngine;
using System.Collections;

public class MoveStomper : MonoBehaviour {

    public float speed;

    private float transY;
    private float length;

    private Vector3 startPosition;

    
    // Use this for initialization
	void Start () {
        this.length = 1.3f;
        startPosition = transform.position;

   
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        //transY = this.length - Mathf.PingPong(Time.time, this.length*2);
        //transY -= length;
        //transform.position = startPosition - new Vector3(0, transY , 0);
        //transform.Translate(0, transY * Time.deltaTime, 0);

        transY = -Mathf.Sin(Time.time * speed) * length;
        transform.position = startPosition - new Vector3(0, Mathf.Abs(transY), 0);
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log(gameObject.name + " collided with: " + other.gameObject.name);
    }
}
