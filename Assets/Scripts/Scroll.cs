﻿using UnityEngine;
using System.Collections;

public class Scroll : MonoBehaviour
{
    public enum ScrollStates {OneWay, WibblyWobbly, Auto, Follow, CamRelative};
    public ScrollStates scrollState;

    
    public float velocity;
    public float length;
    public float finalPosX;
    public float factor;
    public float speed;

    public GameObject target;
    
    private float playerPosX;
    public float distanceOnStart;

    public float distanceToTarget;

    private Vector3 startPosition;

    private Vector3 localPos;

    // Use this for initialization
    void Start()
    {
        startPosition = transform.position;
        localPos = transform.localPosition;

        if (target)
        {
            //Debug.Log(gameObject.name + " has target: " + target.gameObject.name);
            PlayerStats ps = target.GetComponent<PlayerStats>();
            //float temp = target.GetComponent<PlayerStats>().distanceToGoal;
            if (ps)
            {
                //Debug.Log(ps.gameObject.name + " distance to goal: " + ps.distanceToGoal);
                distanceOnStart = ps.distanceToGoal;
               // Debug.Log("-> " + gameObject.name + ": target " + target.gameObject.name + " has playerstats. distance to goal = " + distanceOnStart);
            }

            distanceToTarget = transform.position.x - target.transform.position.x;
        }
        
        //Debug.Log("go pos:" + transform.position.x);
        //Debug.Log("target pos:" + Camera.main.transform.position.x);
    }

    // Update is called once per frame
    void Update()
    {
        //Debug.Log(target.gameObject.name + " distance to goal: " + target.GetComponent<PlayerStats>().distanceToGoal);
        
        //transform.Translate(-Input.GetAxis("Horizontal") * velocity * Time.deltaTime, 0, 0);
        switch(scrollState)
        {
            case ScrollStates.OneWay:
                transform.Translate(velocity * Time.deltaTime, 0, 0);
                break;
            case ScrollStates.WibblyWobbly:
                //velocity = Mathf.PingPong(Time.time, length);
                //float help = -(length / 2) + velocity;
                //transform.Translate(help * Time.deltaTime, 0, 0);
                float transX = -Mathf.Sin(Time.time * speed) * length;
                transform.position = startPosition - new Vector3(Mathf.Abs(transX),0, 0);
                break;
            case ScrollStates.Auto:
                if (target)
                {
                    PlayerStats ps = target.GetComponent<PlayerStats>();

                    if (ps)
                        transform.position = new Vector3(finalPosX - (ps.distanceToGoal * finalPosX / distanceOnStart) * factor, 0, 0);
                }
                break;
            case ScrollStates.CamRelative:
                if (target)
                {
                    PlayerStats ps = target.GetComponent<PlayerStats>();


                    if (ps)
                        transform.localPosition = new Vector3(localPos.x + finalPosX - (ps.distanceToGoal * finalPosX / distanceOnStart) * factor, localPos.y, localPos.z);
                }
                break;
            case ScrollStates.Follow:
               // float bla = player.transform.position.x 
                if(target)
                    transform.position = new Vector3(target.transform.position.x + distanceToTarget, 0, 0);
                break;
            default:
                break;
        }

    }
}
