﻿using UnityEngine;
using System.Collections;

public class ScaleGUITexture : MonoBehaviour {

	// Update is called once per frame
	void Update () {
        guiTexture.pixelInset = new Rect(-Screen.width / 2, -Screen.height / 2, Screen.width, Screen.height);
	
	}
}
