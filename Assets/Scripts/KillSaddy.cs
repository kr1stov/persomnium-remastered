﻿using UnityEngine;
using System.Collections;

public class KillSaddy : MonoBehaviour {

    public AudioClip killSoundBall;
    public AudioClip killSoundFalling;
    private GameObject currentSpawnPoint;

    private bool isDead = false;


    void Start()
    {
        currentSpawnPoint = GameObject.Find("Respawn0");
    }

	void OnCollisionEnter2D(Collision2D other)
    {
        Debug.Log("COL");
        if(other.gameObject.tag == "Ball" && isDead == false)
        {
            isDead = true;
            audio.pitch = 1;
            audio.PlayOneShot(killSoundBall);
            InitDeath(5f);
        }
    }



    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.name == "DeathTrap")
        {
            isDead = true;
            audio.pitch = 1;
            audio.PlayOneShot(killSoundFalling);
            InitDeath(5);
            
            //GetComponent<Platformer2DUserControl>().enabled = false;
            //transform.position = GetLastRespawnPoint().transform.position;
            //GetComponent<Platformer2DUserControl>().enabled = true;
            
            //gameObject.SetActive(false);
        }

        if (other.tag == "Respawn")
        {
            if(other.transform.position.x > currentSpawnPoint.transform.position.x)
            {
                currentSpawnPoint = other.gameObject;
            }
        }

    }

    //void OnCollisionEnter2D(Collision2D other)
    //{
    //    if (other.gameObject.tag == "FallingItem")
    //    {
    //        InitDeath();
    //    }

    //}


    GameObject GetLastRespawnPoint()
    {
        GameObject lastSpawnpoint;
        float minDistance = 999;
        GameObject[] respawns = GameObject.FindGameObjectsWithTag("Respawn");
        lastSpawnpoint = respawns[0];

        foreach (GameObject r in respawns)
        {
            if (r.transform.position.x < transform.position.x)
            {
                float temp = Vector3.Distance(transform.position, r.transform.position);
                if (temp < minDistance)
                {
                    lastSpawnpoint = r;
                    minDistance = temp;
                }
            }
        }


        return lastSpawnpoint;
    }

    void InitDeath(float time)
    {
        Debug.Log("DEAD");
        //audio.pitch = 1;
        //audio.PlayOneShot(killSound);

        PlayerPrefs.SetFloat("psr_lastSpawnX", currentSpawnPoint.transform.position.x);
        PlayerPrefs.SetFloat("psr_lastSpawnY", currentSpawnPoint.transform.position.y);

        GetComponent<Platformer2DUserControl>().enabled = false;
        transform.FindChild("Sprites").gameObject.SetActive(false);
        
        Invoke("Respawn", time);
        //ScreenOverlay temp = Camera.main.GetComponent<ScreenOverlay>();
        //Debug.Log(temp.intensity);
    }

    void Respawn()
    {
        transform.FindChild("Sprites").gameObject.SetActive(true);
        //GetComponent<Platformer2DUserControl>().enabled = false;
        transform.position = currentSpawnPoint.transform.position;
        Debug.Log(currentSpawnPoint.name);
        GetComponent<Platformer2DUserControl>().enabled = true;
        isDead = false;
    }
}
