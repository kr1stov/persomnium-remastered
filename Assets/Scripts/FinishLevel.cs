﻿using UnityEngine;
using System.Collections;

public class FinishLevel : MonoBehaviour {

	void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player")
        {
            InitEnd();
        }
    }

    void InitEnd()
    {
        Application.LoadLevel("End");
    }
}
