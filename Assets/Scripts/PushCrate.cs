﻿using UnityEngine;
using System.Collections;

public class PushCrate : MonoBehaviour {

    Transform frontCheck;
    Transform backCheck;
    Animator anim;

    public bool isPushing = false;

    float frontRadius = .1f;							
    bool inFrontOf = false;

    [SerializeField] LayerMask whatIsMovable;			// A mask determining what is ground to the character	

    RaycastHit2D hitInfo;													


    void Awake()
    {
        // Setting up references.
        frontCheck = transform.Find("FrontCheck");
        backCheck = transform.Find("BackCheck");
        anim = transform.FindChild("Sprites").GetComponent<Animator>();
    }

    void FixedUpdate()
    {
        // The player is grounded if a circlecast to the groundcheck position hits anything designated as ground
        //inFrontOf = Physics2D.OverlapCircle(frontCheck.position, frontRadius, whatIsMovable);
        
        float direction =  frontRadius * Mathf.Sign(Input.GetAxis("Horizontal"));

        hitInfo = Physics2D.Linecast(new Vector2(transform.position.x, transform.position.y), new Vector2(transform.position.x + direction, transform.position.y), whatIsMovable);
        Debug.DrawLine(transform.position, transform.position + new Vector3(direction, 0, 0), Color.green);

        //Debug.Log(hitInfo.transform.name + " on layer: " + hitInfo.transform.tag);


        if( hitInfo && hitInfo.transform.tag == "Movable")
        {
            if (Input.GetButton("Push"))
            {
                hitInfo.rigidbody.mass = 15;
                anim.SetBool("Push", true);
            }
            else
            {
                hitInfo.rigidbody.mass = 100;
                anim.SetBool("Push", false);

            }
        }


    }

    

}
