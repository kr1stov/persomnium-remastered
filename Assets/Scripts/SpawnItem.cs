﻿using UnityEngine;
using System.Collections;

public class SpawnItem : MonoBehaviour
{

    public GameObject[] items;
    private bool canSpawn;

    void Start()
    {
        canSpawn = true;
    }

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.tag == "Player" && canSpawn)
        {
            int r = Random.Range(0, items.Length-1);

            GameObject temp = (GameObject) Instantiate(items[r], transform.position, Quaternion.EulerAngles(0, 0, Random.Range(-10, 10))) as GameObject;
            temp.SendMessage("DoIt");
            canSpawn = false;
        }

    }
}
	
