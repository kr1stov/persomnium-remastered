﻿using UnityEngine;
using System.Collections;

public class DeactivateComponent : MonoBehaviour {

    void DeactivateCollider()
    {
        collider2D.enabled = false;
    }

    void DoIt()
    {
        Invoke("DeactivateCollider", 1f);
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            other.gameObject.SendMessage("InitDeath");
        }

    }
}
