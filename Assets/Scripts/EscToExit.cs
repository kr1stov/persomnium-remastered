﻿using UnityEngine;
using System.Collections;

public class EscToExit : MonoBehaviour {

    private string currentLevel;
    // Use this for initialization
	void Start () {
        currentLevel = Application.loadedLevelName;
	}
	
	// Update is called once per frame
	void Update () {
	    if(Input.GetKeyDown(KeyCode.Escape))
        {
            switch(currentLevel)
            {
                case "End":
                    Application.LoadLevel("Credits");
                    break;
                case "Credits":
                    Application.LoadLevel("Menu");
                    break;
                case "Intro":
                    Application.LoadLevel("Menu");
                    break;
                default:
                    Application.LoadLevel("Menu");
                    break;
            }
        }
	}
}
