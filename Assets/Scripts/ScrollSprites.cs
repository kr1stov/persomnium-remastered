﻿using UnityEngine;
using System.Collections;

public class ScrollSprites : MonoBehaviour {

    public Transform[] layers;
    public float[] velocities;

    private Transform player;
    private float startPosX;
    private float distanceFromStart;
    private float distanceToGoal;

    private float currentX;
    private float lastX;

    private float deltaX;
    // Use this for initialization
	void Start () {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        lastX = player.position.x;
        currentX = lastX;
        deltaX = currentX - lastX;
        //startPosX = player.position.x;
        //distanceFromStart = player.position.x - startPosX;
	}
	
	// Update is called once per frame
	void Update () {
        currentX = player.position.x;
        deltaX = currentX - lastX;
        //distanceFromStart = player.position.x - startPosX;

        
        for(int i=0; i<layers.Length; i++)
        {
            deltaX = deltaX * velocities[i] * Time.deltaTime;
            layers[i].position -= new Vector3(deltaX, 0f, 0f);
            //layers[i].Translate(-Input.GetAxis("Horizontal") * velocities[i] * Time.deltaTime, 0, 0);
        }

        lastX = currentX;
	}
}
