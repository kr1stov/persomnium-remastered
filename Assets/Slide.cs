﻿using UnityEngine;
using System.Collections;

public class Slide : MonoBehaviour {

    Animator anim;

	// Use this for initialization
	void Start () {
        anim = transform.FindChild("Sprites").GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D other)
    {
        if(other.gameObject.tag == "Slide")
        {
            anim.SetBool("Slide", true);
        }
    }
    
    void OnCollisionStay2D(Collision2D other)
    {
        if (other.gameObject.tag == "Slide")
        {
            anim.SetBool("Slide", true);
        }
    }


    void OnCollisionExit2D(Collision2D other)
    {
        if (other.gameObject.tag == "Slide")
        {
            anim.SetBool("Slide", false);
        }
    }
}
